# BeeRecord
_Where "ActiveRecord" encounters "HoneySQL"..._

[![Clojars Project](https://img.shields.io/clojars/v/bee-record.svg)](https://clojars.org/bee-record)
[![Build Status](https://travis-ci.org/mauricioszabo/bee-record.svg?branch=master)](https://travis-ci.org/mauricioszabo/bee-record)

BeeRecord is an wrapper to HoneySQL. It generates queries from a "mapping" - a simple Clojure map that will map entities into tables, and make associations between these entities/tables with joins. The syntax for join is exactly the same as HoneySQL `:join` key, so there's no conversion (and you can join things as complex as you want).

Heavily inspired by Ruby's ActiveRecord (and Sequel), it allows us to generate queries (in HoneySQL), to query things with its associations (with the `sql` namespace). Different from ActiveRecord and Sequel, it will not try to abstract SQL for you, it allows you to configure complex joins and associations, and there may be no 1-1 mapping only - you can map the same table to multiple entities, and you can also map multiple tables to a single entity.

## Introduction

You first must create a mapping. It is just a Clojure map containing at least `:entities`, but you'll probably want `:joins` too:

```clojure
(def mapping {:entities {:person :people
                         :pet :pets
                         :record :medicalrecord}
              :joins {[:person :pet] [:= :pets.people_id :people.id]
                      [:pet :record] [:= :pets.id :medicalrecord.pet_id]}})
```

Then, we can query things:

```clojure
(require '[bee-record.walker :as walker])

(def to-honey (walker/parser-for mapping))
(to-honey {:select [:person/name :pet/name]})
; => {:select [[:people.name "person/name"]]}
;              [:pets.name "pet/name"]]
;     :from [:people]
;     :join [:pets [:= :pets.people_id :people.id]]}
```

## Why?
SQL is **hard**. Ok, let's rephrase that: SQL is not _that hard_, but it becomes harder and harder the more you need conditional SQL. Let's begin by the simpler example possible: you want to filter users on your system. To filter it, you need to concatenate SQL fragments depending on what the fields the user wants to filter, or add joins, or transform a join from inner to left... then things become complicated, messy, and when you see you're just fighting with string concatenation instead of working the logic in your app.

Clojure already have a wonderful library that creates a single SQL query for us: it's HoneySQL. It's a wonderful library that expects a clojure map and creates a query pair (string + fields) that we can send to `clojure.java.jdbc/query`. It's possible to work with HoneySQL alone, and it's already a million times better than working without it:

```clojure
(require '[honeysql.core :as honey]
         '[clojure.java.jdbc :as jdbc])

(def people {:from [:people] :select [:id :name]})
(jdbc/query db (honey/format people :quoting :ansi))
=> [{:id 1, :name "Foo"}
    {:id 2, :name "Bar"}
    {:id 3, :name "Baz"}]
```

Then, if you want to filter something, you just `assoc` on the `people` var a `:where` clause, and everything works. But this covers _one query only_. If we want to use multiple queries (preloads, for instance), we're out of luck. Also, we still need to join by hand, and unless you make an opinated function that derives your foreign keys and primary keys, then do the joins, you'll have to type more.

So, the idea of BeeRecord is to have two different namespaces: one that you can write most, if not all, the SQL (HoneySQL, in this case) boilerplate for you, and the other that makes the queries (with Next.JDBC). The "queries" namespace also is responsible for doing preloads, and doing it in the least amount of queries possible - by using Pathom, and generating resolvers "on the fly", we're able issue queries and use the already-powerful Pathom library to care about which queries need to run first, which ones can run in parallel, etc.

The `sql` namespace is responsible for transforming a "mapping" into queries. So, for example, for the mapping above, you could query with this:

```clojure
(require '[bee-record.sql :as sql])

(def q (sql/queries-for mapping))
; This will query all people, with their pets. May return duplicates,
; if a person have more than one pet, or ignore a person if the person
; have no pets
(q [{:person [:person/name :pet/name]}])

; This will query all people, with their pets as a sub-element:
(q [{:person [:person/name {:pet [:pet/name]}]}])

; And this will do the same query as above, but filter only users that
; are over 18 years old:
(q [{:person ['(:person/name {:where [:> :person/age 18]})
              {:pet [:pet/name]}]}])
```

For now, only EQL is supported, with some limitations:

1. "Root" queries are not supported. You can't query `[:person/name]`, for example. The reason is quite simple: you can't return a single row without any kind of condition
    1. If you want somekind of "root" query, you'll need to query a map, with the key being a vector. Like: `[{[:person/id 1] [:person/name]}]`. Please notice that BeeRecord is strict: if the query returns more than a single row, it'll throw an exception;
    1. You can't query a "join" with the same format above
1. You can query sub-elements of that table by asking for a sub-entity (like the pet example, above), and it'll try to find a path between `person` and `pet`. You can "force" the path by using qualified keywords - for example, instead of `[{:person [... {:pet ...}]}`, you can use `[{:person [... {:person/pet ...}]}`

## State

More testing is needed - it's currently only a proof-of-concept. Use for personal projects, but be cautious if using in production _specially_ if you're thinking about generating the queries dynamically.

Also, sometimes queries can make no sense: for example, if you don't have a linear path between multiple entities, you can end up with a cartesian product of the queries (kinda like if you use `join` in SQL in the incorrect way). BeeRecord tries to find the most linear path, but sometimes this is simply not possible. It's up to you to find the right path.

## License

Copyright © 2018 Maurício Szabo

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
