(ns bee-record.walker
  (:require [clojure.spec.alpha :as s]
            [loom.graph :as graph])
  (:import [clojure.lang MapEntry IMapEntry]))

(defn- walker-alias? [elem]
  (and (keyword? elem) (namespace elem)))

(defn- get-all-tables [query]
  (let [deep-flatten #(->> %
                           flatten
                           ; (tree-seq coll? seq)
                           (filter walker-alias?))
        fields (concat (-> query :select deep-flatten)
                       (-> query :where deep-flatten)
                       (-> query :having deep-flatten)
                       (-> query :group-by deep-flatten)
                       (-> query :order-by deep-flatten))
        tables (->> fields
                    (mapcat #(some-> %  walker-alias? keyword vector)))
        first-table (first tables)]
    {:first-table first-table
     :other-tables (-> tables set (disj first-table))}))

(defn- alias-for-attribute [mapping entity-name]
  (let [[kind table-like] (-> mapping :entities entity-name)]
    (assert table-like (str "Entity not found for key " entity-name))
    (case kind
      :alias (name table-like)
      :table (-> table-like :alias name)
      (name entity-name))))

(declare parser-for)

(defn- to-table [raw-mapping mapping entity-name]
  (let [[kind table-like] (-> mapping :entities entity-name)]
    (assert table-like (str "Entity not found for key " entity-name))
    (case kind
      :alias table-like
      :table [(:table table-like) (:alias table-like)]
      :union (let [parse (-> raw-mapping
                             (update :entities dissoc entity-name)
                             parser-for)
                   entities (->> table-like
                                 :subentities
                                 (map parse))]
               [(cons (:union-kind table-like) entities)
                (name entity-name)]))))

(defn- find-all-paths [g start end visited]
  (if (= start end)
    [[end]]
    (for [succ (graph/successors g start)
          :when (not (visited start))
          path (find-all-paths g succ end (conj visited start))]
      (cons start path))))

(defn cart [colls]
  (if (empty? colls)
    '(())
    (for [more (cart (rest colls))
          x (first colls)]
      (cons x more))))

(defn- rank [paths]
  (let [size (->> paths (filter identity) distinct count)]
    (if (< size 10)
      (str "0" size)
      (str size))))

(defn- unique-rank [multi-branch-path]
  (->> multi-branch-path
       (map rank)
       (reduce str)))

(defn- compare-paths [paths]
  (let [size (->> paths (map count) (apply max))
        normalized (map #(take size (concat % (repeat nil))) paths)]
    (apply map vector normalized)))

(defn- make-join [raw-mapping mapping joins last-entities curr-entities]
  (let [joins-made (atom #{})]
    (->> curr-entities
         (map vector last-entities)
         (reduce (fn [acc pair]
                   (let [to-join (second pair)]
                     (if (or (@joins-made pair) (nil? to-join))
                       acc
                       (do
                         (swap! joins-made conj pair)
                         (conj acc
                               (->> to-join (to-table raw-mapping mapping))
                               (or (get joins (set pair))
                                   (throw (ex-info "Can't find join for tables"
                                                   {:tables (set pair)}))))))))
                 []))))

(defn- filter-via [join-via all-paths]
  (if (seq join-via)
    (filter (fn [path]
              (let [requireds (volatile! join-via)]
                (doseq [joins path
                        :when (some #{(first @requireds)} joins)]
                  (vswap! requireds rest))
                (empty? @requireds)))
            all-paths)
    all-paths))

(defn- make-joins [raw-mapping mapping graph first-table tables join-via]
  (let [compared-paths (->> tables
                            (map #(find-all-paths (:g graph) first-table % #{}))
                            cart
                            (map compare-paths)
                            (filter-via join-via))
        most-reusable-path (->> compared-paths (sort-by unique-rank) first)]
    (when (empty? most-reusable-path)
      (throw (ex-info "No path found for query" {:join-via join-via
                                                 :tables (cons first-table tables)})))

    (loop [visited-entities #{first-table}
           [last-entities curr-entities & next-entities] most-reusable-path
           all-joins []]
      (cond
        (some visited-entities curr-entities)
        (throw (ex-info "Can't make this path - an entity appears in multiple sides of the join"
                        {:entity-to-join (some visited-entities curr-entities)
                         :joins-so-far all-joins}))

        curr-entities
        (let [joins (make-join raw-mapping mapping (:joins graph)
                                last-entities curr-entities)]
          (recur
            (apply conj visited-entities curr-entities)
            (cons curr-entities next-entities)
            (apply conj all-joins joins)))

        :else
        all-joins))))

(defn- walk
  "Specific version of clojure.walk/walk"
  [inner outer form]
  (cond
    (list? form) (outer (apply list (map inner form)))
    (instance? IMapEntry form)
    (outer (MapEntry/create (inner (key form)) (inner (val form))))
    (seq? form) (outer (doall (map inner form)))
    (instance? clojure.lang.IRecord form)
    (outer (reduce (fn [r x] (conj r (inner x))) form form))

    (coll? form)
    (if (-> form meta :walker/keep)
      form
      (outer (into (empty form) (map inner form))))

    :else (outer form)))

(defn- postwalk [f form]
  (walk (partial postwalk f) f form))

(defn norm-where [mapping wheres]
  (postwalk (fn [elem]
              (if-let [table (walker-alias? elem)]
                (keyword (str (->> table keyword (alias-for-attribute mapping))
                              "."
                              (name elem)))
                elem))
            wheres))

(defn- norm-field [mapping field]
  (if-let [table-from-q (walker-alias? field)]
    (let [table (alias-for-attribute mapping (keyword table-from-q))
          field (name field)]
      (keyword (str table "." field)))
    field))

(defn- aliased-attribute [mapping field]
  (let [field-name (name field)
        alias (str (namespace field) "/" field-name)
        table-from-q (walker-alias? field)]
    (if-not table-from-q
      alias
      (let [table (alias-for-attribute mapping (keyword table-from-q))
            field-part (keyword (str table "." field-name))]
        (if (= "*" field-name)
          field-part
          [field-part alias])))))

(defn- norm-select [mapping select]
  (let [norm-field #(norm-field mapping %)]
    (mapv #(if (keyword? %)
             (aliased-attribute mapping %)
             (postwalk norm-field %))
          select)))

(s/def ::union
  (s/cat :union-kind #{:union :union-all}
         :subentities (s/+ map?)))

(s/def ::entity (s/or
                 :alias keyword?
                 :union (s/spec ::union)
                 :table (s/cat :table any? :alias keyword?)))

(s/def ::entities (s/map-of keyword? ::entity))

(s/def ::join-tables (s/cat :first keyword? :second keyword?))
(s/def ::joins (s/map-of ::join-tables any?))
(s/def ::mapping (s/keys :req-un [::entities ::joins]))

(defn parser-for [raw-mapping]
  (s/assert* ::mapping raw-mapping)
  (let [mapping (s/conform ::mapping raw-mapping)
        graph (reduce (fn [graph [[f1 f2] condition]]
                        (-> graph
                            (update :joins assoc (set [f1 f2]) condition)
                            (update :g graph/add-path f1 f2)))
                      {:g (graph/graph)}
                      (:joins mapping))]

    (with-meta
      (fn [query]
        (assert (map? query))
        (let [{:keys [first-table other-tables]} (get-all-tables query)
              seed-query (-> query
                             (assoc :select (->> query :select (norm-select mapping))
                                    :from [(to-table raw-mapping mapping first-table)])
                             (dissoc :join-via))]
          (cond-> seed-query
            (not-empty other-tables)
            (assoc :join (make-joins raw-mapping mapping graph first-table other-tables
                                     (:join-via query)))

            (:where query)
            (assoc :where (norm-where mapping (:where query)))

            (:having query)
            (assoc :having (norm-where mapping (:having query)))

            (:group-by query)
            (assoc :group-by (postwalk #(norm-field mapping %) (:group-by query)))

            (:order-by query)
            (assoc :order-by (postwalk #(norm-field mapping %) (:order-by query))))))
      {:graph graph, :mapping mapping, :raw-mapping raw-mapping})))

#?(:clj
   (defn dot-str [parser]
     (when-let [dot-str (requiring-resolve 'loom.io/dot-str)]
       (-> parser meta :graph :g dot-str))))
