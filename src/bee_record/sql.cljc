(ns bee-record.sql
  (:require [com.wsscode.pathom3.interface.eql :as eql]
            [com.wsscode.pathom3.connect.operation :as connect]
            [com.wsscode.pathom3.connect.indexes :as idx]
            [clojure.walk :as walk]
            [honey.sql :as honey]
            [clojure.string :as str]
            [bee-record.walker :as walker]
            [next.jdbc.sql :as jdbc-sql]
            [next.jdbc.result-set :as rs]))

(defn query! [conn query]
  (jdbc-sql/query conn query {:builder-fn rs/as-unqualified-maps}))

(defn query->indexes [acc entity expected-attributes]
  (let [attributes (remove map? expected-attributes)
        sub-elems (filter map? expected-attributes)
        entity-name (cond-> entity (list? entity) first)]
    (reduce (fn [acc map]
              (reduce (fn [acc [k vs]] (query->indexes acc k vs))
                acc
                map))
            (update acc entity-name concat attributes)
            sub-elems)))

(defn- join-fields-for-entity [rev-entities parser entity-name]
  (let [from (namespace entity-name)
        to (name entity-name)
        {:keys [from join]} (parser {:select [(keyword from "_whatever")
                                              (keyword to "_whatever")]})
        origin (-> from first name)]
    (for [[_ join-condition] (partition 2 join)
          join-field (flatten join-condition)
          :let [parts (-> join-field str (subs 1) (str/split #"[\.\/]"))
                table (->> parts pop (str/join "."))
                field (peek parts)]
          :when (= table origin)
          :let [entity (rev-entities table)]]
      (keyword entity field))))

(defn- full-index-map [acc join-fields]
  (reduce (fn [acc [key]]
            (if (qualified-keyword? key)
              (let [fields (join-fields key)
                    orig-entity (-> key namespace keyword)]
                (update acc orig-entity concat fields))
              acc))
          acc
          acc))

(defn- generate-batch-where [inputs]
  (some->> inputs
           (mapcat (fn [condition]
                     (some->> condition
                              (map (fn [[k v]] [:= k v]))
                              not-empty
                              (cons :and)
                              vector)))
           not-empty
           (cons :or)))


(defn- current-time []
  #?(:clj (java.util.Date.) :cljs (js/Date.)))

(defn- root-resolver [parser attrs]
  (fn [{:keys [conn log] :as env} inputs]
    (let [params (connect/params env)
          input-fields (-> inputs first keys)
          where (generate-batch-where inputs)
          query (parser (-> params
                            (assoc :select (concat attrs input-fields))
                            (update :where #(if %
                                              [:and % where]
                                              where))))
          sql (honey/format query)
          inst (current-time)
          _ (when log (log {:kind :query :query sql :inst inst}))
          results (group-by #(select-keys % input-fields) (query! conn sql))
          inst-end (current-time)]

      (when log (log {:kind :query-end :query sql :inst inst-end
                      :time (- (inst-ms inst-end)
                               (inst-ms inst))}))

      (map (fn [input-keyseq]
             (let [res (get results input-keyseq [])]
               (if (-> res count (= 1))
                 (first res)
                 (throw (ex-info "More than 1 result returned" {:count (count res)})))))
           inputs))))

(defn- batch-resolver [parser attrs resolver-key]
  (fn [{:keys [conn log] :as env} inputs]
    (let [params (update (connect/params env) :join-via #(some-> % reverse))
          input-fields (-> inputs first keys)
          where-from-inputs (generate-batch-where inputs)
          partial-query (if where-from-inputs
                          (update params :where #(if %
                                                     [:and % where-from-inputs]
                                                     where-from-inputs))
                          params)
          query (parser (assoc partial-query :select (concat attrs input-fields)))
          sql (honey/format query)
          inst (current-time)
          _ (when log (log {:kind :query :query sql :inst inst}))
          results (group-by #(select-keys % input-fields) (query! conn sql))
          inst-end (current-time)]

      (when log (log {:kind :query-end :query sql :inst inst-end
                      :time (- (inst-ms inst-end)
                               (inst-ms inst))}))
      (map (fn [input-keyseq]
             (let [res (get results input-keyseq [])]
               (if (list? resolver-key)
                 (->> resolver-key
                      (map (fn [k] [k res]))
                      (into {}))
                 {resolver-key res})))
           inputs))))

(defn- create-resolver [parser join-fields [resolver-key resolver-attrs]]
  (let [attrs (-> resolver-attrs distinct vec)
        inputs (cond
                 (list? resolver-key) (-> resolver-key first join-fields)
                 (qualified-keyword? resolver-key) (join-fields resolver-key)
                 (vector? resolver-key) (subvec resolver-key 0 1))]
    (connect/resolver (symbol (str resolver-key))
                      {::connect/input (vec inputs)
                       ::connect/output (cond
                                          (vector? resolver-key) attrs
                                          (list? resolver-key) (mapv (fn [k]
                                                                       {k attrs})
                                                                     resolver-key)
                                          :else [{resolver-key attrs}])
                       ::connect/batch? true}
                      (cond
                        (vector? resolver-key) (root-resolver parser attrs)
                        :else (batch-resolver parser attrs resolver-key)))))

(defn- rename-keyword [parent k renames]
  (let [name-part (name k)
                  new-name (keyword (str "__RENamed__" parent) name-part)
                           parent (cond-> parent (list? parent) first)]
       (swap! renames assoc new-name {:unnamespaced k
                                      :namespaced (keyword (name parent) name-part)})
    new-name))

(defn- namespace-all-subqueries [query parent renames]
  (let [res
        (mapv (fn [elem]
                (if (map? elem)
                  (->> elem
                       (map (fn [[k v]]
                              (let [[subvs] (namespace-all-subqueries
                                             v
                                             (cond
                                               (list? k) (first k)
                                               (vector? k) (-> k first namespace keyword)
                                               :else k)
                                             renames)]
                                (cond
                                  (nil? parent)
                                  [k subvs]

                                  (simple-keyword? k)
                                  [(rename-keyword parent k renames) subvs]

                                  (and (list? k) (-> k first simple-keyword?))
                                  (let [[kword & condition] k
                                        new-name (rename-keyword parent kword renames)]
                                    [(apply list new-name condition) subvs])

                                  :else
                                  [k subvs]))))
                       (into {}))
                  elem))
              query)]
    [res @renames]))

(defn- append-renames [config renames]
  (reduce (fn [config [rename {:keys [namespaced]}]]
            (-> config
                (dissoc namespaced)
                (assoc (list namespaced rename) (config namespaced))))
          config
          renames))

(defn- prepare-indexes [query parser]
  ;; FIXME: this is simply wrong, that's now how we get entities at all...
  (let [[query renames] (namespace-all-subqueries query nil (atom {}))
        rev-entities (->> parser meta :raw-mapping :entities
                          (map (fn [[k v]] [(name v) (name k)]))
                          (into {}))
        join-fields (memoize #(join-fields-for-entity rev-entities parser %))
        query-with-renamed (walk/postwalk #(if-let [e (get-in renames [% :namespaced])]
                                             e
                                             %)
                                          query)
        indexes-config (-> {}
                           (query->indexes nil query-with-renamed)
                           (dissoc nil)
                           (full-index-map join-fields)
                           (append-renames renames))
        indexes (map #(create-resolver parser join-fields %) indexes-config)]
    {:idx (idx/register indexes)
     :renames renames
     :new-query query}))

(defn queries-for [mapping]
  (let [parser (walker/parser-for mapping)]
    (fn [params query]
      (let [{:keys [idx renames new-query]} (prepare-indexes query parser)]
        (-> idx
            (merge params)
            (eql/process new-query)
            (->> (walk/postwalk #(if-let [res (get-in renames [% :unnamespaced])]
                                   res
                                   %))))))))
