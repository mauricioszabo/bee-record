(defproject bee-record "0.2.0"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [com.github.seancorfield/honeysql "2.1.818"]
                 [aysylu/loom "1.0.2"]
                 [com.wsscode/pathom3 "2021.10.20-alpha"]
                 [com.github.seancorfield/next.jdbc "1.2.731"]]

  :profiles {:dev {:dependencies [[check "0.2.1-SNAPSHOT"]
                                  [org.hsqldb/hsqldb "2.4.0"]]}})
