# Change Log

## 0.2.0
- A HUGE rethink of the project
- Walkers now work correctly
- `sql` framework is different - rewrote in terms of Pathom and Walker
- Walker mapping now accepts "union" - it basically calculates another path, using the
same parser "minus" the union-ed entity, to generate interesting edge-cases
- JOIN path is completely rewroten. Now, it'll try to re-use paths so it'll probably behave better
- Better behave with custom entities, aliases, etc
- `:entity/*` is now supported

## 0.1.0
- Associations now generate scopes
- Scopes can be used with `with` aggregation
- Aggregations on scopes
- Nested aggregations
- Fixed a bug on tables
- Added `walker` namespace

## 0.0.5
### Added
- Simple queries like `select`, `where`, `restrict`, and `join`
- `model` to generate a "Bee Model", that'll ease queries
- `query` and `find` to resolve queries
- `to-sql` to show the query that'll be used
