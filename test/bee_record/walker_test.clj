(ns bee-record.walker-test
  (:require [clojure.test :refer [deftest testing]]
            [check.core :refer [check]]
            [bee-record.walker :as walker]
            [matcher-combinators.test]))

(def mapping {:entities {:person :people
                         :pet :pets
                         :record :medicalrecord}
              :joins {[:person :pet] [:= :pets.people_id :people.id]
                      [:pet :record] [:= :pets.id :medicalrecord.pet_id]}})

(def q (walker/parser-for mapping))

(deftest basic-queries
  (testing "generates a simple query"
    (check (q {:select [:person/id :person/name]})
           => {:select [[:people.id "person/id"] [:people.name "person/name"]]
               :from [:people]}))

  (testing "re-alias an attribute if needed"
    (check (q {:select [[:person/id "some/id"] [:person/name "person/name2"]]})
           => {:select [[:people.id "some/id"] [:people.name "person/name2"]]
               :from [:people]}))

  (testing "don't alias `*`"
    (check (q {:select [:person/*]})
           => {:select [:people.*]
               :from [:people]})))

(def parse (walker/parser-for mapping))

(deftest generation-of-a-honey-query
  (testing "generation of a query with a direct join"
    (check (parse {:select [:person/name :pet/color :pet/race]})
           => {:select [[:people.name "person/name"]
                        [:pets.color "pet/color"]
                        [:pets.race "pet/race"]]
               :from [:people]
               :join [:pets [:= :pets.people_id :people.id]]}))

  (testing "generation of a query with indirect joins"
    (check (parse {:select [:person/name :record/sickness]})
           => {:select [[:people.name "person/name"] [:medicalrecord.sickness "record/sickness"]]
               :from [:people]
               :join [:pets [:= :pets.people_id :people.id]
                      :medicalrecord [:= :pets.id :medicalrecord.pet_id]]}))

  (testing "will normalize fields on sql functions"
    (check (parse {:select [[[:count :person/id]]]})
           => {:select [[[:count :people.id]]]
               :from [:people]}))

  (testing "will normalize group, order, and having"
    (check (parse {:select [:pet/id]
                   :distinct? true
                   :group-by [:pet/name]
                   :order-by [:person/id]})
           => {:select [[:pets.id "pet/id"]]
               :distinct? true
               :from [:pets]
               :join [:people [:= :pets.people_id :people.id]]
               :group-by [:pets.name]
               :order-by [:people.id]})))

(deftest generation-of-queries-with-filters
  (testing "adding joins when filtering for fields on another entity"
    (check (parse {:select [:person/name] :where [:= :pet/name "Rex"]})
           => {:select [[:people.name "person/name"]]
               :from [:people]
               :join [:pets [:= :pets.people_id :people.id]]
               :where [:= :pets.name "Rex"]}))

  (testing "will not normalize subfields with specific metadata"
    (check (parse {:select [:person/name]
                   :where [:or
                           [:in :pet/name ["Rex" "Dog"]]
                           [:in :pet/name ^:walker/keep {:select [:common/pets]
                                                         :from [:common/names]}]]})
           => {:select [[:people.name "person/name"]]
               :from [:people]
               :join [:pets [:= :pets.people_id :people.id]]
               :where [:or
                       [:in :pets.name ["Rex" "Dog"]]
                       [:in :pets.name {:select [:common/pets]
                                        :from [:common/names]}]]})))

(deftest non-entity-fields
  (testing "will be ignored on the select clause"
    (check (parse {:select [["wow" :some-str] :person/name :pet/name]})
           => {:select [["wow" :some-str] [:people.name "person/name"] [:pets.name "pet/name"]]
               :from [:people]
               :join [:pets [:= :pets.people_id :people.id]]})))

(deftest union-entities
  (let [map (assoc-in mapping
                       [:entities :person-and-pet]
                       [:union
                        {:select [[:person/id "pid"] :person/*]}
                        {:select [[:person/id "pid"] :pet/*]}])
        parse (walker/parser-for map)]
    (check (parse {:select [:person-and-pet/pid :person-and-pet/name]})
           => {:select [[:person-and-pet.pid "person-and-pet/pid"]
                        [:person-and-pet.name "person-and-pet/name"]]
               :from [[[:union
                        {:select [[:people.id "pid"] :people.*]
                         :from [:people]}
                        {:select [[:people.id "pid"] :pets.*]
                         :from [:people]
                         :join [:pets [:= :pets.people_id :people.id]]}]
                       "person-and-pet"]]})))

(def complex-parse (walker/parser-for
                    {:entities {:person :people
                                :child [:people :children]
                                :toy :toys
                                :preferred [:toys :preferred]
                                :house :houses}
                     :joins {[:house :person] [:= :people.house_id :houses.id]
                             [:person :child] [:= :children.parent_id :people.id]
                             [:child :toy] [:= :children.id :toys.child_id]
                             [:child :preferred] [:= :children.id :preferred.child_id]}}))

(deftest complex-queries
  (check (complex-parse {:select [:house/id :child/name]
                         :where [:and
                                 [:= :toy/name "Car"]
                                 [:= :preferred/name "Doll"]]})
         => {:select [[:houses.id "house/id"] [:children.name "child/name"]]
             :from [:houses]
             :join [:people [:= :people.house_id :houses.id]
                    [:people :children] [:= :children.parent_id :people.id]
                    :toys [:= :children.id :toys.child_id]
                    [:toys :preferred] [:= :children.id :preferred.child_id]]
             :where [:and
                     [:= :toys.name "Car"]
                     [:= :preferred.name "Doll"]]}))

(def multi-path-mapping
  {:entities {:root :root
              :from-path :path
              :end :end}
   :joins {[:root :end] [:= :root.id :end.root_id]
           [:root :from-path] [:= :root.id :path.root_id]
           [:from-path :end] [:= :path.end_id :end.id]}})

(deftest multi-path-to-element
  (testing "by default, gets the smallest path to the entity"
    (let [parser (walker/parser-for multi-path-mapping)]
      (check (parser {:select [:root/id :end/id]})
             => {:select [[:root.id "root/id"] [:end.id "end/id"]]
                 :from [:root]
                 :join [:end [:= :root.id :end.root_id]]})))

  ;; FIXME: Somehow, we need a way to configure "aliases" for entities, so that
  ;; a path like :root/id -> :end/id can be aliased to :end-from-root/id and, then,
  ;; we can have :end-from-root/id -> :other-path/id without repeating every :end->*
  ;; joins for :end-from-root.
  #_
  (testing "allow an alias to re-use the same paths for specific joins"))

(deftest keep-join-in-single-line
  (testing "when two paths would get to the same target, choose one that will be re-used
            in future joins"
    (let [parser (walker/parser-for {:entities {:a :a :b :b :c :c :d :d}
                                     :joins {[:a :c] :_
                                             [:a :b] :_
                                             [:b :c] :_
                                             [:b :d] :_}})]
      (check (parser {:select [:a/id :c/id :d/id]})
             => {:select vector?
                 :join [:c :_
                        :b :_
                        :d :_]})))

  (testing "sort by longest path (user->permissions vs user->roles->permissions)"
    (let [parser (walker/parser-for {:entities {:user :user
                                                :up :up
                                                :permission :permission
                                                :role :role
                                                :rp :rp}
                                     :joins {[:user :up] :short
                                             [:up :permission] :short
                                             [:user :role] :long
                                             [:role :rp] :long
                                             [:rp :permission] :long}})]
      (check (parser {:select [:user/id :role/id :permission/id]})
             => {:select vector?
                 :join [:role :long
                        :rp :long
                        :permission :long]})))

  (testing "customize the path"
    (let [parser (walker/parser-for {:entities {:user :user
                                                :up :up
                                                :permission :permission
                                                :role :role
                                                :rp :rp}
                                     :joins {[:user :up] :short
                                             [:up :permission] :short
                                             [:user :role] :long
                                             [:role :rp] :long
                                             [:rp :permission] :long}})]
      (check (parser {:select [:user/id :permission/id]
                      :join-via [:role :permission]})
             => {:select vector?
                 :join [:role :long
                        :rp :long
                        :permission :long]})

      (testing "but crashes if can't find path"
        (check (parser {:select [:user/id :permission/id]
                        :join-via [:role :permission :user]})
               =throws=> clojure.lang.ExceptionInfo)))))
