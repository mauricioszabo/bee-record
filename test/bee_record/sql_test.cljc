(ns bee-record.sql-test
  (:require [bee-record.sql :as sql]
            [clojure.test :refer [deftest testing]]
            [check.core :refer [check]]
            [matcher-combinators.matchers :as m]
            [honey.sql :as honey]
            [next.jdbc :as jdbc]
            [next.jdbc.sql :as jdbc-sql]))

(defn- create-tables! [conn]
  (jdbc-sql/query conn (honey/format {:create-table :people
                                      :with-columns [[:id :integer [:raw "primary key"]]
                                                     [:name [:varchar 255]]
                                                     [:age :integer]]}))

  (jdbc-sql/query conn (honey/format {:create-table :users
                                      :with-columns [[:id :integer [:raw "primary key"]]
                                                     [:person_id :integer]
                                                     [:login [:varchar 255]]]}))

  (jdbc-sql/query conn (honey/format {:create-table :users_permissions
                                      :with-columns [[:user_id :integer]
                                                     [:permission_id :integer]]}))

  (jdbc-sql/query conn (honey/format {:create-table :roles_permissions
                                      :with-columns [[:permission_id :integer]
                                                     [:role_id :integer]]}))

  (jdbc-sql/query conn (honey/format {:create-table :roles
                                      :with-columns [[:id :integer [:raw "primary key"]]
                                                     [:user_id :integer]]}))

  (jdbc-sql/query conn (honey/format {:create-table :permissions
                                      :with-columns [[:id :integer [:raw "primary key"]]
                                                     [:name [:varchar 255]]]})))

(defn- seed-data! [conn]
  (jdbc-sql/query conn (honey/format {:insert-into :people
                                      :values [{:id 1 :name "Person Admin" :age 30}
                                               {:id 2 :name "Person User" :age 30}
                                               {:id 3 :name "Person Guest" :age 30}]}))

  (jdbc-sql/query conn (honey/format {:insert-into :users
                                      :values [{:id 1 :login "admin" :person_id 1}
                                               {:id 2 :login "example" :person_id 1}
                                               {:id 3 :login "user" :person_id 2}
                                               {:id 4 :login "guest" :person_id 3}]}))

  (jdbc-sql/query conn (honey/format {:insert-into :permissions
                                      :values [{:id 1 :name "FROM_ROLE"}
                                               {:id 2 :name "FROM_USER"}]}))

  (jdbc-sql/query conn (honey/format {:insert-into :users_permissions
                                      :values [{:user_id 1 :permission_id 2}
                                               {:user_id 3 :permission_id 2}]}))

  (jdbc-sql/query conn (honey/format {:insert-into :roles_permissions
                                      :values [{:permission_id 1 :role_id 1}]}))

  (jdbc-sql/query conn (honey/format {:insert-into :roles
                                      :values [{:id 1 :user_id 1}]})))

(defmacro prepare-db [ & body]
  `(let [~'conn (jdbc/get-datasource {:jdbcUrl "jdbc:hsqldb:mem:bee-test", "", ""})]
     (try
       (honey/set-dialect! :ansi :quoted true)
       (create-tables! ~'conn)
       (seed-data! ~'conn)
       ~@body
       (finally
         (jdbc-sql/query ~'conn ["SHUTDOWN COMPACT"])))))

(def mapping {:entities {:person :people
                         :user :users
                         :user-permission :users_permissions
                         :role :roles
                         :role-permission :roles_permissions
                         :permission :permissions}
              :joins {[:person :user] [:= :people.id :users.person_id]
                      [:user :user-permission] [:= :users.id :users_permissions.user_id]
                      [:user :role] [:= :users.id :roles.user_id]
                      [:role :role-permission] [:= :roles.id :roles_permissions.role_id]
                      [:permission :user-permission] [:= :permissions.id :users_permissions.permission_id]
                      [:permission :role-permission] [:= :permissions.id :roles_permissions.permission_id]}})

(def q (sql/queries-for mapping))

(deftest root-query
  (testing "root query"
    (prepare-db
     (check (q {:conn conn} [{:person [:person/name :user/login]}])
            => {:person (m/in-any-order
                         [{:person/name "Person Admin" :user/login "admin"}
                          {:person/name "Person Admin" :user/login "example"}
                          {:person/name "Person User" :user/login "user"}
                          {:person/name "Person Guest" :user/login "guest"}])})))

  (testing "root queries with filters"
    (prepare-db
     (check (q {:conn conn} [{'(:person {:where [:= :person/id 1]}) [:person/name :user/login]}])
            => {:person (m/in-any-order
                         [{:person/name "Person Admin" :user/login "admin"}
                          {:person/name "Person Admin" :user/login "example"}])})))

  (testing "root query by index"
    (prepare-db
     (check (q {:conn conn} [{[:user/id 1] [:person/name :user/login]}])
            => {[:user/id 1] {:person/name "Person Admin"
                              :user/login "admin"}})))

  (testing "fails if finds more than 1 elem"
    (prepare-db
     (check
      (q {:conn conn} [{[:person/id 1] [:person/name :user/login]}])
      =throws=> clojure.lang.ExceptionInfo))))

(deftest joining
  (testing "querying for children"
    (prepare-db
     (check (q {:conn conn} [{'(:person {:where [:= :person/id 1]})
                              [:person/name {:person/user [:user/login]}]}])
            => {:person [{:person/name "Person Admin"
                          :person/user (m/in-any-order
                                        [{:user/login "admin"}
                                         {:user/login "example"}])}]})))

  (testing "querying for multiple children"
    (prepare-db
     (check (q {:conn conn} [{'(:person {:where [:< :person/id 3]})
                               [:person/name {:person/user [:user/login]}]}])
            => {:person (m/in-any-order
                         [{:person/name "Person Admin"
                           :person/user (m/in-any-order
                                         [{:user/login "admin"}
                                          {:user/login "example"}])}
                          {:person/name "Person User"
                           :person/user [{:user/login "user"}]}])})))

  (testing "querying children that will be resolved from different paths"
    (prepare-db
     (check (q {:conn conn} [{'(:user {:where [:= :user/id 1]})
                               [:user/login
                                {:user/permission [:permission/name]}
                                {'(:user/role {:join-via [:role :permission]})
                                 [:permission/name]}]}])
            => {:user [{:user/login "admin"
                        :user/permission [{:permission/name "FROM_USER"}]
                        :user/role [{:permission/name "FROM_ROLE"}]}]}))))

(deftest join-without-namespace
  (testing "will find the right origin, and join it"
    (prepare-db
     (check (q {:conn conn} [{'(:person {:where [:= :person/id 1]})
                              [:person/name {:user [:user/login]}]}])
            => {:person [{:person/name "Person Admin"
                          :user (m/in-any-order
                                 [{:user/login "admin"}
                                  {:user/login "example"}])}]})))

  (testing "will filter the join"
    (prepare-db
     (check (q {:conn conn} [{'(:person {:where [:= :person/id 1]})
                              [:person/name {'(:user {:where [:!= :user/login "example"]})
                                             [:user/login]}]}])
            => {:person [{:person/name "Person Admin"
                          :user [{:user/login "admin"}]}]})))

  (testing "will filter the join, and query root by ID"
    (prepare-db
     (check (q {:conn conn} [{[:person/id 1]
                              [:person/name {'(:user {:where [:!= :user/login "example"]})
                                             [:user/login]}]}])
            => {[:person/id 1]
                {:person/name "Person Admin"
                 :user [{:user/login "admin"}]}})))

  ;; FIXME: waiting for https://github.com/wilkerlucio/pathom3/issues/105
  #_
  (testing "will filter batch joins"
    (prepare-db
     (check (q {:conn conn :log println} [{[:person/id 1]
                                           [:person/name {'(:user {:where [:= :user/login "admin"]})
                                                          [:user/login]}]}
                                          {[:person/id 2]
                                           [:person/name {'(:user {:where [:= :user/login "user"]})
                                                          [:user/login]}]}])
            => {[:person/id 1]
                {:person/name "Person Admin"
                 :user [{:user/login "admin"}]}
                [:person/id 2]
                {:person/name "Person User"
                 :user [{:user/login "user"}]}}))))

(deftest batching-queries
  (testing "makes the less amount of queries possible"
    (prepare-db
     (let [queries (atom 0)
           log (fn [{:keys [kind]}] (when (= :query kind) (swap! queries inc)))]
       (check (q {:conn conn :log log}
                 [{[:person/id 2] [:person/name {:user [:user/login]}]}
                  {[:person/id 3] [:person/name {:user [:user/login]}]}])
              => {[:person/id 2] {:person/name "Person User"
                                  :user [{:user/login "user"}]}
                  [:person/id 3] {:person/name "Person Guest"
                                  :user [{:user/login "guest"}]}})
       (check @queries => 2)))))
